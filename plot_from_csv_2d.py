import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import matplotlib.pyplot as plt
import numpy
import numpy as np


# no; x; y; dx; dy; predictionX; predictionY; predictionDx; predictionDy; correctionX; correctionY; correctionDx; correctionDy;
# csv = np.genfromtxt('/Users/Air/kalman_data_2d_result.csv', delimiter=";")
# x_data = csv[:, 1]
# y_data = csv[:, 2]
# dx_data = csv[:, 3]
# dy_data = csv[:, 4]
# predictionX = csv[:, 5]
# predictionY = csv[:, 6]
# predictionDx = csv[:, 7]
# predictionDy = csv[:, 8]
# correctionX = csv[:, 9]
# correctionY = csv[:, 10]
# correctionDx = csv[:, 11]
# correctionDy = csv[:, 12]

# plt.plot(x_data, y_data, correctionX, correctionY)
# plt.show()

correction = True

import matplotlib.pyplot as plt
plt.ion()
class DynamicUpdate():
    #Suppose we know the x range
    min_x = 0
    max_x = 320

    def on_launch(self):
        #Set up plot
        self.figure, self.ax = plt.subplots()
        self.lines, = self.ax.plot([],[],'.')
        # plt.setp(self.lines, color='r', linewidth=2.0, self.lines, color='g',linewidth=2.0)

        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        self.ax.set_xlim(self.min_x, self.max_x)
        #Other stuff
        self.ax.grid()
        # ...

    def on_running(self, xdata, ydata):
        #Update data (with the new _and_ the old points)
        # if (correction):
        #     plt.setp(self.lines, 'color', 'r', 'linewidth', 2.0)
        self.lines.set_xdata(xdata)
        self.lines.set_ydata(ydata)
        # self.lines.set_xdata(correctionXdata)
        # self.lines.set_ydata(correctionYdata)

        #Need both of these in order to rescale
        self.ax.relim()
        self.ax.autoscale_view()
        #We need to draw *and* flush

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    #Example
    @property
    def __call__(self):
        import numpy as np
        import time
        csv = np.genfromtxt('/Users/Air/kalman_data_2d_result.csv', delimiter=";")
        x_data = csv[:, 1]
        y_data = csv[:, 2]
        dx_data = csv[:, 3]
        dy_data = csv[:, 4]
        predictionX = csv[:, 5]
        predictionY = csv[:, 6]
        predictionDx = csv[:, 7]
        predictionDy = csv[:, 8]
        correctionX = csv[:, 9]
        correctionY = csv[:, 10]
        correctionDx = csv[:, 11]
        correctionDy = csv[:, 12]
        self.on_launch()
        xdata = []
        ydata = []
        correctionXdata = []
        correctionYdata = []
        for n in range(len(correctionX)):
            xdata.append(x_data[n])
            ydata.append(y_data[n])
            correctionXdata.append(correctionX[n])
            correctionYdata.append(correctionY[n])
            self.on_running(xdata, ydata)

            # self.on_running(correctionXdata, correctionYdata, True)
            time.sleep(0.5)

        time.sleep(10)
        return xdata, ydata

d = DynamicUpdate()
d()