#!/usr/bin/env python
import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as etree
#
# requests.post('https://msesandbox.cisco.com:443/api/contextaware/v1/maps/info/Moscone/MosconeWest/DevNetLive',
#                   auth=HTTPBasicAuth('learning', 'learning'))

r=requests.get('https://msesandbox.cisco.com:443/api/contextaware/v1/location/clients/00:00:2a:01:00:09',
               auth=HTTPBasicAuth('learning', 'learning'))


tree = etree.fromstring(r.text)
print(tree)
print(tree.tag)
print(r.text)

for child in tree.findall('MapCoordinate'):
     print(child.tag, child.attrib)
     y_coordonate = child.get('y')
     x_coordinate = child.get('x')
     print(x_coordinate, y_coordonate)


# with open('/Users/Air/data.xml', 'w') as f:
#     f.write(r.text)