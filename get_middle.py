#!/usr/bin/env python
# You are going to be given a word. Your job is to return the middle character of the word.
# If the word's length is odd, return the middle character.
# If the word's length is even, return the middle 2 characters.
#
# Examples:
#    getMiddle("test") should return "es"
#    getMiddle("testing") should return "t"
#    getMiddle("middle") should return "dd"
#    getMiddle("A") should return "A"
# Input
#    A word (string) of length 0 < str < 1000
#
# Output
#    The middle character(s) of the word represented as a string.

def get_middle(s):
    n = len(s)
    if n == 1: return s
    if n%2 == 0: return s[int(n/2) - 1: int(n/2) + 1]
    if n%2 == 1: return s[int(n/2) : int(n/2)+1]



print(get_middle("test") == "es")
print(get_middle("A") == "A")
print(get_middle("middle") == "dd")
print(get_middle("testing") == "t")
print(get_middle("of") == "of")