#!/usr/bin/env python

class Dog():
	def __init__(self, dogname, dogcolor, dogheight, dogbuild, dogmood, dogage):
		self.name = dogname
		self.color = dogcolor
		self.height = dogheight
		self.build = dogbuild
		self.mood = dogmood
		self.age = dogage
		self.Hungry = False
		self.Tired = False

	def Eat(self):
		if self.Hungry:
			print 'Mmm Mmm.. I want to eat!'
			self.Hungry = False
		else:
			print 'Fuck off!'

	def Sleep(self):
		if self.Tired:
			print 'ZZZZZZZZZZZZZ...'
			self.Tired = False

	def Bark(self):
		if (self.mood == 'Svarliviy'):
			print 'RRRRRrrrr...Gav! Gav!'
		elif self.mood == 'Spokonyi':
			print 'ohhhmm...OK...Gav!'
		elif self.mood == 'Crazy':
			print 'AvAVVAvAvAvAVAVAvA..!'
		else:
			print 'Gav!'


Beagle = Dog('Archy', 'Blue', 'Low', 'Big', 'Svarliviy', 12)

print 'My name is %s' % Beagle.name
print 'My color is %s' % Beagle.color
print 'My mood is %s' % Beagle.mood
print 'Am I Hungry = %s' % Beagle.Hungry

Beagle.Eat()
Beagle.Hungry = True
Beagle.Eat()
Beagle.Bark()
